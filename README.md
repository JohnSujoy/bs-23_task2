# README #

# OS Requirement: Ubuntu 20.04 LTS #

Need to install Vagrant on host machine

Need to install Ansible on host machine

# STEP-1 Install Vagrant #

Update and install package

$ sudo apt update

$ sudo apt install virtualbox curl -y

Download the Vagrant package

$ curl -O https://releases.hashicorp.com/vagrant/2.2.9/vagrant_2.2.9_x86_64.deb

Once the file is downloaded, Install Vgrant

$ sudo apt install ./vagrant_2.2.9_x86_64.deb

Check Vagrant version

$ vagrant --version

# STEP-2 Install Ansible #

Update and install ansible package

$ sudo apt update $sudo apt install ansible -y

Check Ansible version

$ ansible --version

# STEP-3 Fork public git repository #

Update and install git package

$ sudo apt update

$ sudo apt install git -y

Clone Public repo

$ git clone https://JohnSujoy@bitbucket.org/JohnSujoy/bs-23_task2.git

$ cd bs-23_task2

# STEP-4 Spin up 1 master and 2 worker kubernetes cluster using Vagrant (CNI calico.)#

$ vagrant up

# STEP-5 Test and Verify k8s cluster on master#

$ vagrant ssh k8s-master

$ kubectl get nodes
